'use strict';

var os = require('os'),
	gulp = require ('gulp'),
	gutil = require("gulp-util"),
	clean = require ('gulp-clean'),
	download = require ("gulp-download-stream"),
	wait = require ('gulp-wait'),
	fileExists = require ('file-exists'),
	concat = require('gulp-concat'),
	sourcemaps = require('gulp-sourcemaps'),
	minifyHTML = require('gulp-minify-html'),
	less = require('gulp-less'),
    minifyCSS = require('gulp-clean-css'),
    browserify = require('gulp-browserify'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	imageop = require('gulp-image-optimization'),
	argv = require('yargs').argv,
	gulpif = require('gulp-if'),
	shell = require('gulp-exec'),
	replace = require('gulp-batch-replace'),
	gmcfp = require('gulp-mysql-command-file-processor');
	
module.exports = {
	js: function (params) {
	
		if (params.local) {
		
			console.error ("Not Fetching JS...");
			if (params.finish)
				params.finish ();
				
			return gulp.src (params.dest);
		} else {
		
			var url = 'http://lamp.liquidint.com/build/liquid.js?client=' + params.client;
			if (params.version)
				url = 'http://lamp.liquidint.com/build/v' + params.version + '/liquid.js?client=' + params.client;
		
			//gulp.src (params.dest + "/liquid.js").pipe (clean ());
			download({
			  file: "liquid.js",
			  url: url
			}).pipe (gulp.dest (params.dest)).on ('finish', function () {
			
				var check = 0;
				while (check<1) {
					console.info ("Checking for JS...");
					if (fileExists (params.dest + "/liquid.js")) {
						check = 1;
					} else {
						console.error ("Waiting for JS...");
						check = 0;
					}
				}
			
				//if (params.ie) {
				
					var url = 'http://lamp.liquidint.com/build/liquid-ie9.js';
					if (params.version)
						url = 'http://lamp.liquidint.com/build/v' + params.version + '/liquid-ie9.js';
				
					//gulp.src (params.dest + "/liquid-ie9.js").pipe (clean ());
					download({
					  file: "liquid-ie9.js",
					  url: url
					}).pipe (gulp.dest (params.dest)).on ('finish', function () {
					
					
						var check = 0;
						while (check<1) {
							console.info ("Checking for IE9 JS...");
							if (fileExists (params.dest + "/liquid-ie9.js")) {
								check = 1;
							} else {
								console.error ("Waiting for IE9 JS...");
								check = 0;
							}
						}
					
						if (params.finish)
							return params.finish ();
							
						return gulp.src (params.dest);
					
					});
			}).on ("error", function () {
				console.error ("(Error encountered) Not Fetching JS...");
				if (params.finish)
					return params.finish ();
					
				return gulp.src (params.dest);
			});
		}
	},
	
	less: function (params) {
		
		if (params.local) {
		
			console.error ("Not Fetching LESS...");
			if (params.finish)
				params.finish ();
				
			return gulp.src (params.dest);
		} else {
		
			var url = 'http://lamp.liquidint.com/build/liquid.less?client=' + params.client;
			if (params.version)
				url = 'http://lamp.liquidint.com/build/v' + params.version + '/liquid.less?client=' + params.client;
		
			//gulp.src (params.dest + "/liquid.less").pipe (clean ());
			download({
			  file: "liquid.less",
			  url: url
			}).pipe (gulp.dest (params.dest)).on ('finish', function () {
			
				var check = 0;
				while (check<1) {
					console.info ("Checking for LESS...");
					if (fileExists (params.dest + "/liquid.less")) {
						check = 1;
					} else {
						console.error ("Waiting for LESS...");
						check = 0;
					}
				}
			
				//if (params.ie) {
				
					var url = 'http://lamp.liquidint.com/build/liquid-ie9.less';
					if (params.version)
						url = 'http://lamp.liquidint.com/build/v' + params.version + '/liquid-ie9.less';
				
					//gulp.src (params.dest + "/liquid-ie9.less").pipe (clean ());
					download({
					  file: "liquid-ie9.less",
					  url: url
					}).pipe (gulp.dest (params.dest)).on ('finish', function () {
					
					
						var check = 0;
						while (check<1) {
							console.info ("Checking for IE9 LESS...");
							if (fileExists (params.dest + "/liquid-ie9.less")) {
								check = 1;
							} else {
								console.error ("Waiting for IE9 LESS...");
								check = 0;
							}
						}
					
						if (params.finish)
							return params.finish ();
							
						return gulp.src (params.dest);
					
					});
			}).on ("error", function () {
				console.error ("(Error encountered) Not Fetching JS...");
				if (params.finish)
					return params.finish ();
					
				return gulp.src (params.dest);
			});
		}
	}
};
